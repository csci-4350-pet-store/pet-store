﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net.Http;
using System.Threading.Tasks;

namespace PetStoreAPI.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class MonitoringController : Controller
    {

        private HttpClient client = new HttpClient();
        private string basePath = "http://34.125.193.123/BigBrotherRedux";
        [HttpGet("pages")]
        public async Task<IActionResult> Pages(string date, string description)
        {
            if (!string.IsNullOrEmpty(date) && !string.IsNullOrEmpty(description))
            {
                var data = await client.GetStringAsync($"{basePath}/PageReference/CreatePageRefrence/{date}/{description}");

            }

            return Ok();
        }
    }
}
