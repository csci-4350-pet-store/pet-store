﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PetStoreAPI.Models.Entities;
using PetStoreAPI.Services.Interfaces;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace PetStoreAPI.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    [SwaggerTag("Create, read and delete Users")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _user;

        public UserController(IUserRepository user)
        {
            _user = user;
        }

        [HttpPost("one/{id}")]
        [SwaggerOperation(
    Summary = "Gets one user",
    Description = "Returns a single user",
    OperationId = "OneUser",
    Tags = new[] { "User" }
    )]
        [SwaggerResponse(200, "Returns One user", typeof(User))]
        [SwaggerResponse(404, "Specified user wasn't found", null)]
        public async Task<IActionResult> GetOne( int? id, User user)
        {
           
           

            if (id > 0)
            {
                user = await _user.ReadAsync(user.Username, user.Password, id);
               
                return Ok(user);
               
            }

           
            user = await _user.ReadAsync(user.Username, user.Password);
            if (user == null)
            {
                ModelState.AddModelError("password", "Wrong username or password");
            }

            if (ModelState.IsValid)
            {
                return Ok(user);
            }
            var errorList = ModelState.ToDictionary(
           kvp => kvp.Key,
           kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
           );
            return Ok(errorList);

           
        }

        [HttpPost("create")]
        [SwaggerOperation(
    Summary = "Creates a user",
    Description = "Posts the user to database",
    OperationId = "CreateUser",
    Tags = new[] { "User" }
    )]
       
        public async Task<IActionResult> Post(User user)
        {
         
            if(String.IsNullOrEmpty(user.Username))
            {
                ModelState.AddModelError("username", "Username is required");

            }
            if (String.IsNullOrEmpty(user.Password))
            {
                ModelState.AddModelError("password", "Password is required");
            }
            if (String.IsNullOrEmpty(user.FirstName))
            {
                ModelState.AddModelError("firstname", "Firstname is required");
            }
            if(String.IsNullOrEmpty(user.LastName))
            {
                ModelState.AddModelError("lastname", "Lastname is required");
            }
         


            if (ModelState.IsValid)
            {
                await _user.CreateAsync(user);
               
                HttpContext.Response.Cookies.Append("uid", user.Id.ToString());
                return CreatedAtAction("Get", new { id = user.Id }, user);
            }
            var errorList = ModelState.ToDictionary(
             kvp => kvp.Key,
             kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
             );
            return Ok(errorList);
        }

       

      

        [HttpDelete("delete")]
        [SwaggerOperation(
    Summary = "Deletes a user",
    Description = "Removes the user from the database",
    OperationId = "DeleteUser",
    Tags = new[] { "User" }
    )]
        [SwaggerResponse(204, "Successful specified user was deleted")]
        public async Task<IActionResult> Remove()
        {
            var userId = Convert.ToInt32(HttpContext.Request.Cookies["uid"]);
            await _user.DeleteAsync(userId);
            return NoContent();
        }
    }
}
